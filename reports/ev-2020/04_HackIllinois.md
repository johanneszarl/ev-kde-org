During the weekend of February 28 to March 1, I attended the 2020 [HackIllinois](https://www.hackillinois.org/) event in Champaign-Urbana as a FOSS mentor, representing KDE.

The event is a hackathon in which students work with experienced open source mentors over 36 hours to contribute to open source through new features, bug fixes, and documentation changes.

I'd like to present my after-action report:

### Overview

First the good news: the KDE team won!

My students reported that the judges were impressed with their results, excitement, and passion, and the fact that [one of the submitted patches](https://invent.kde.org/kde/konsole/-/merge_requests/68) has already been merged.

My students principally worked on building [a visualizer for plasma-pa's microphone audio input level](<https://bugs.kde.org/show_bug.cgi?id=411563>) and managed to put together [a pretty decent proof-of-concept](https://phabricator.kde.org/F8146046) ([code here](https://github.com/NSLeung/KDE-Neon-HackIllinois2020/commits/joey_branch)). The code is not in a merge-worthy state, but could definitely get there eventually.

They also submitted some nice smaller patches: the aforementioned Konsole fix, and [one for Dolphin too](https://phabricator.kde.org/D27757). They also made [a thorough investigation of a significant Yakuake issue](https://bugs.kde.org/show_bug.cgi?id=389448) that has been affecting two of them. Two of the students in particular seem quite eager to continue their contributions.

### Promo & Social Observations

Nobody had an unkind or negative word for KDE. People who had heard of us really seemed to love us.

The other FOSS mentors at the event who I talked to had all heard of KDE and some had used [Plasma](https://kde.org/plasma-desktop/) in the past or still do. While most of the students I talked to had never heard of KDE, most of the ones who had been already using a Plasma-based distro (mostly [KDE Neon](https://neon.kde.org/), with some Manjaro)! Several GNOME-using students were impressed by what they saw and eager to help out, and the students already using KDE software were super duper enthusiastic. Most had never filed any bug reports or submitted patches, but eagerly jumped into this. They did not find the process of doing so especially difficult, so I suspect that a lack of outreach was principally what had kept them from doing so before.

Students were especially impressed with [Yakuake](https://apps.kde.org/yakuake/), the embedded terminals in [Dolphin](https://apps.kde.org/dolphin/) and [Kate](https://kate-editor.org/), and Plasma itself. They all thought it was very attractive and polished-looking.

### Onboarding & Technical Observations

Overall, the process of setting up a KDE development environment from scratch was not a major pain point, especially for the Linux-using students. However, a number of build failures took a lot of time to investigate and teach people how to work around. Please help to keep the master branches of your projects compilable with default CMake settings, common compilers, and easily installable dependencies, everyone!

The students using Apple laptops had to set up their development environments in virtual machines due to a lack of macOS support in our current developer tooling and documentation. I had them install Neon Developer Edition, which worked fine overall, but it occurred to me that this edition would be more useful for its stated purpose if it shipped with a pre-generated .kdesrc-buildrc config file, plus kdesrc-build itself and all necessary dependencies listed in the [Guidelines](https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source/Install_the_dependencies#KDE_neon.2C_Debian.2C_Ubuntu.2C_Kubuntu).

These enhancements would have yielded significant time savings for my VM-using students.

### Hardware observations

From my observations, at least 70% of the students attending the event were using Apple hardware running macOS. Most of the remaining students were using non-Apple hardware running some flavor of Linux, about a 60/40 mix of Plasma and GNOME, respectively. I did not see a single student using a PC running any version of Windows.

Overall it felt like a worthwhile endeavor! But after all the excitement and 36 hours of mentoring non-stop, I needed a good night's sleep...