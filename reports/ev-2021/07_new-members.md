KDE e.V. welcomed the following new members in 2021:

* Janet Blackquill
* Arjen Hiemstra
* Ismael Asensio de la Fuente
* Kurt Hindenburg
* Camilo Higuita Rodriguez
* Stefan Gerlach
* Matthieu Gallien
* Johannes Zarl-Zierl
* Tobias Fella
* Devin Lin
* Ahmad Samir
* Noah Davis
* Alexander Semke
* Waqar Ahmed
* Alexander Lohnau
* Luis Falcon
* Bhavisha Dhruve
