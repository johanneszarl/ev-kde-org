KDE’s work is made possible thanks to the contributions from KDE Community
members, donors and companies that support us. In 2023, we launched a major
fundraising campaign that turned out to be a resounding success.

### Plasma 6 Fundraiser

The [Plasma 6 Fundraiser](https://kde.org/fundraisers/plasma6member/) was
initiated in anticipation of the highly awaited Plasma 6 release, scheduled for
February 2024. The campaign’s original goal was to increase the number of KDE
e.V. supporting members from 50 under the old CiviCRM system to 500. However,
the response far exceeded expectations, with over 1,000 new supporting members
and over 127,000€ in recurring donations raised. Aside from providing a
stable source of recurring incomes, this allowed us to finally drop the old
CiviCRM system by early 2024.

In addition to recurring contributions, we also raised around €45,000 in
one-time donations — an impressive 75% increase compared to 2022.
