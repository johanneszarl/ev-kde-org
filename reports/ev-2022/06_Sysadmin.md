The 2022 year was an incredibly busy year for Sysadmin, with many long awaited projects being delivered.  
  
The most visible of these was the general availability of full CI services natively within Gitlab, which had been partially completed last year. This was subsequently followed by support for Qt 6 on several platforms to support the coming transition from Qt 5 to Qt 6. Work to scope what is needed for Gitlab to replace the Binary Factory was also completed.  
  
Improvements were also made to our Single Sign On setup, with a significant number of services migrating from KDE Identity to our new Gitlab based setup with only a small number of services left to migrate. We also significantly upgraded the server that hosts our BigBlueButton instance, allowing for larger online meetings to be held at any time.  
  
The replacement of older systems also picked up pace, with our bulk storage system being replaced and preparations being made for the replacement of our primary web servers and email server. The continued migration to Gitlab authentication and these server replacements promise a busy year ahead for 2023.