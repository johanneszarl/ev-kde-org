<figure class="image-right"> <img width="100%" src="images/Aleix.jpg" alt="Picture of Aleix Pol" /> </figure>

Looking back on 2022, one of the highlights for me was the opportunity to come together in person. While our technical work is always important, the human aspect of our enterprise defines who we are. After two editions Akademy online, it was great to catch up with each other, share some laughs, and of course, discuss how we can improve our software to better serve our users.

From a very different perspective, we achieved a significant milestone in the *Make a Living in KDE* process ([as explained in this presentation by Lydia and Neofytos](https://www.youtube.com/watch?v=MzpoTuHuciE>)). Throughout 2022, we started the hiring process for the three positions that kicked off this plan, and in 2023, I am looking forward to reaping the fruit of our hard work. With this effort, we hope to improve the overall contribution experience in KDE and help more people benefit from our software.

As per tradition, after three years we chose [a new set of goals](https://kde.org/goals/) for the KDE community. These goals serve as a rallying point, giving us all something to work towards and providing a clear path to improve ourselves. The three new goals revolve around accessibility, software sustainability, and internal process automation. I strongly recommend that everyone take a look and consider how to contribute to these important initiatives.

Looking ahead into 2023, there's no shortage of excitement for us either. We have expanded our reach to millions of handheld console users which has become an entirely new userbase to serve. We are also upgrading our stack and jumping into a new series of Qt software which brings a good number of opportunities. And, last but definitely not least, we have each other to do this with.

Allons-y, KDE!
