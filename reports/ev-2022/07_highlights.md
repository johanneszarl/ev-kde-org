Like in previous reports, this isn’t in any way, shape, or form a list of everything that happened in KDE during 2022; it’s just an overview of the big things we noticed happening during the year.

### Hardware Partnerships

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Highlights/hardware.png" alt="4 slices of KDE-powered hardware: A TUXEDO computers laptop, a KDE Slimbook 4 ultrabook, a Kubuntu Focus laptop, and the Steam Deck" />
    </figure>
</div>

Overall 2022 was a good year for getting KDE software into more people’s hands through hardware.

Linux hardware vendor [TUXEDO Computers](https://www.tuxedocomputers.com) started shipping Plasma by default on all of their machines.

[KFocus](https://kfocus.org/), makers of Kubuntu-based hardware, added some new machines to their line-up, all shipping KDE Plasma and apps by default.

Slimbook released a new version of their [KDE Slimbook laptop](https://slimbook.es/kde-slimbook-amd)The KDE Slimbook IV improves on the previous model, which was already excellent.

Finally, the [Steam Deck](https://www.steamdeck.com) became a smash hit, selling over a million devices worldwide and introducing as many people to the world of KDE.

### Akademy

For the first time in a few years, we had a real, in-person Akademy. It was so wonderful to reconnect with KDE folks in person. This report contains [a detailed chronicle](#id="supported-activities-developer-sprints-and-conferences_") of what went on, and you can also watch [videos of the sessions and talks here](https://tube.kockatoo.org/w/p/uWamiTUb8vGiiUCUcUddCk).

### Professionalizing KDE

In 2022 KDE e.V. [started hiring engineers for technical positions](https://ev.kde.org/corporate/staffcontractors/), beginning with a packaging engineer and a documentation specialist. Hiring has continued into 2023 as well.

### New Goals

This year, KDE did [a third round of goal-picking](https://community.kde.org/Goals#Current_goals), cementing this process as a key part of KDE’s culture. The three goals chosen were "KDE For All" (accessibility), "Sustainable Software", and "Automate And Systematize Internal Processes". Check out the [*Featured* article](#featured-article-kde-s-goals-of-2022-and-beyond_) above for more details.

### Infrastructure

[KDE's Bugzilla](https://bugs.kde.org/) got a re-organization to make it easier for normal people to figure out where to submit a bug report.

KDE also got a better donation and fundraising platform, [powered by Donorbox](https://donorbox.org/kde-community). This makes it much simpler for people to donate to KDE e.V..

Finally we worked on building [a new forum powered by Discourse](https://discuss.kde.org).

### Qt 6

This year KDE contributors spent an enormous amount of time porting KDE software to Qt 6, the latest version of the Qt toolkit. The work is now more than half done, with most common software and nearly all of Plasma already completed. [You can see the progress here](https://iskdeusingqt6.org/).

### Wayland

Porting KDE software to Wayland made enormous progress in 2023. Slimbook started shipping their new KDE Slimbook laptops with Wayland by default, following Fedora KDE 34 doing the same in late 2021. Our list of showstoppers continued to shrink, and new issues added to it were notably less bad then the ones they replaced. There were discussions about defaulting to Wayland in Plasma 6 in 2023, either for the inaugural release or one of the ones soon after it.

### Plasma Desktop

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Highlights/Plasma_tiling.png" alt="Screenshot of the Plasm desktop showing divisions you can use to set up tiling." />
    </figure>
</div>

Among many other changes, we got custom window tiling layouts; massive stability improvements for multi-monitor workflows; Wayland fractional scaling; non-blurry scaled XWayland apps; a UI overhaul for Discover; many KRunner UX improvements; mouse button re-binding; resizable Panel popups; finger-following touchpad gestures on Wayland; support for alternate calendars such as the Chinese lunar calendar and Islamic civil calendar; picking-and-choosing what you want to apply from Global Themes; accent color generated from the wallpaper’s dominant color; full-window tinting with the accent color; [and a lot more](https://kde.org/announcements).

### Apps

KDE has so many apps that we really can’t possibly do them justice here! Nevertheless, here’s an extremely small assortment:

* [Kdenlive](https://kdenlive.org) developers added many new effects throughout 2022, implemented a new *Guides and Markers* dock, and worked on  integrating Glaxnimate with Kdenlive, giving video editors a powerful new tool of adding animations into their work. In non-technical news, the Kdenlive developers and KDE fundraiser team ran and successfully completed a fundraiser campaign to finance adding some long-planned features and improvements to Kdenlive.

* [Krita](https://krita.org) got a storyboard editor for animations, a recorder to make those cool-looking speed-painting videos, added more operations that can handle multiple layers, and improved the code of KDE's powerful painting and design program across the board.

* [digiKam](https://www.digikam.org/), KDE's image manager and organizer for the professional and keen photographer, updated its code base to increase stability and iron out bugs, added support for more image formats, improved features like face recognition, and updated and expanded its documentation.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Highlights/Dolphin.png" alt="Dolphin, with a split pane and showing the top and bottom green bars of the Selection Mode on the left." />
    </figure>
</div>

* [Dolphin](https://apps.kde.org/dolphin/) got a new *Selection Mode*, a new (optional) list view selection style, the ability to browse iOS devices using their native `afc://` protocol, an eject button in the sidebar list items of ejectable/unmountable volumes.

* [Okular](https://okular.kde.org/) got a welcome screen, a new Breeze icon that better matches the original, a UI overhaul for its sidebar, and was [the first software application awarded the Blaue Engel certification](https://eco.kde.org/blog/2022-03-16-press-release-okular-blue-angel/)

* [Gwenview](https://userbase.kde.org/Gwenview) gained features to annotate images and edit their brightness, contrast, and gamma.

* [Kate and KWrite](https://kate-editor.org/) got welcome screens, KHamburgerMenu support, searchable settings windows, keyboard macro support, and even more massive UX and feature improvements of all kinds due to an influx of new contributors and a higher tempo of regular development work.

* [Konsole](https://konsole.kde.org/) got [Sixel](https://en.wikipedia.org/wiki/Sixel) support, adopted KHamburgerMenu, added a plugin to save and restore text snippets, and moved its tab bar to the top of the view by default.

* [Spectacle](https://apps.kde.org/spectacle/) was ported to Kirigami, given integrated screen recording support on Wayland, and now lets you annotate in Rectangular Region mode.

* [Filelight](https://apps.kde.org/filelight/) was ported to Kirigami and gained a sidebar.

* [Ark](https://apps.kde.org/ark/) got a welcome screen, KHamburgerMenu support, and overhauled toolbar contents.

* [Elisa](https://elisa.kde.org/) gained support for displaying auto-scrolling lyrics from songs using embedded LRC lyrics, .pls playlists, a real Full Screen mode, and improved presentation in Artists view, touchscreen UX improvements and overhauled playlist styling.

* [NeoChat](https://apps.kde.org/neochat/) got encrypted chat support and a boatload of features and UI improvements!
