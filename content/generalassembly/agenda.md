---
title: "Agenda General Assembly KDE e.V. 2024"
layout: page
---

This is the agenda for the Annual General Assembly (AGM) of KDE e.V. for 2024.
It takes place Online on [Saturday 19th of October at 1 pm UTC](https://zonestamp.toolforge.org/1729342800).


## Agenda

1. Welcome
2. Election of the chair of the general assembly
3. Report of the board
   1. Report about activities
   2. Report of the treasurer
   3. Report of the auditors of accounting
   4. Relief of the board
4. Report of representatives, working groups and task forces of KDE e.V.
   1. Report of the representatives to the KDE Free Qt Foundation
   2. Report of the KDE Free Qt Working Group
   3. Report of the System Administration Working Group
   4. Report of the Community Working Group
   5. Report of the Financial Working Group
   6. Report of the Advisory Board Working Group
   7. Report of the Fundraising Working Group
5. Election of the Auditors of Accounting
6. Election of the Representatives to the KDE Free Qt Foundation
7. Miscellaneous
