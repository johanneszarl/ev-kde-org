---
title: 'KDE e.V. Report for 1st Half of 2015'
date: 2016-04-08 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is happy to present the <a href="https://ev.kde.org/reports/ev-2015H1/" target="_blank">report for the first half of 2015</a>.

This report covers January through June 2015, including statements from the board, reports from sprints and a feature article about conf.kde.in 2015.
      
