---
title: 'KDE e.V. is looking for a contractor to coordinate the KDE Goals process'
date: 2024-06-05 15:00:00
layout: post
noquote: true
---

Edit 2024-07-13: applications for this position **are closed**.

KDE e.V., the non-profit organization supporting the KDE community, is looking for a proactive contractor to support and coordinate the KDE Goals process. The role involves providing project management, community engagement, event planning, and other needed services to help drive the success of KDE Goals. Please see the [job ad](/resources/jobad-projectcoordinator2024.pdf) for more details about this contracting opportunity.

We are looking forward to your application.
