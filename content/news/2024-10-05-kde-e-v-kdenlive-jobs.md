---
title: 'KDE e.V. and Kdenlive team are looking for contractors'
date: 2024-10-04 10:00:00
layout: post
noquote: true
---

Edit 2024-10-28: applications for these positions are closed. KDE e.V., the non-profit organization supporting the KDE community, and the Kdenlive team are looking for proactive contractors to implement some features in the Kdenlive video editor. Two positions are currently open:
 - **OpenTimelineIO integration**: this will require implementing a C++ module in Kdenlive to allow importing and exporting using this open standard, to allow exchanging project files with other applications. Please see the [job ad](/resources/jobad-opentimelineio2024.pdf) for more details about this contracting opportunity.

 - **Audiowaveform integration**: this will require rewriting the code used to generate and display the audio waveforms in Kdenlive using the audiowaveform library. This should bring faster and more precise waveforms in the timeline. Please see the [job ad](/resources/jobad-audiowaveform2024.pdf) for more details about this contracting opportunity.
We are looking forward to your application.
