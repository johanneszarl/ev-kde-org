---
title: 'KDE e.V. Quarterly Report 2007Q2'
date: 2007-09-22 00:00:00 
layout: post
---

The <a href="/reports/ev-quarterly-2007Q2.pdf">KDE e.V.
Quarterly Report</a> is now available for April to June 2007. As usual it includes
reports of the board and the working groups about the KDE e.V. activities of the last
quarter and future plans.
