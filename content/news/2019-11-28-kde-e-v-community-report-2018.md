---
title: KDE e.V. Community Report 2018
date: 2019-11-28 00:00:00
layout: post
---

In a large free software community like KDE, something exciting happens every day of the year. 2018 was no exception. In this report, you will find some of the highlights of the year, and learn how KDE e.V. helped to make them happen.

You can find the report here: [KDE e.V. Community Report for 2018](/reports/ev-2018/).
