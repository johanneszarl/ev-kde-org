---
title: 'KDE e.V. Membership Council'
date: 2003-08-01 00:00:00 
slug: kde-e-v-membership-council
---

<a href="http://events.kde.org/info/kastle/">KDE Contributor Conference 2003</a>, followed by an one week hackfest afterwards.