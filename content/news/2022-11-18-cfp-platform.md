---
title: 'KDE e.V. is looking for a software engineer'
date: 2022-11-18 00:00:00
layout: page
menu_active: Organization
noquote: true
slug: cfp-platform
categories: [Hiring]
---

> Edit 2022-12-10: applications for this position **are closed**.

KDE e.V., the non-profit organisation supporting the KDE community, is
looking to hire a software engineer to help improve the software stack that KDE software relies on.
Please see the [call for proposals](/resources/callforproposals-platform2022)
for more details about this contract opportunity.
We are looking forward to your application.

> The full call for proposals has more details.
