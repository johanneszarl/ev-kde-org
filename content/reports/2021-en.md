---
title: "Annual General Meeting KDE e.V. 2021"
layout: page
---

The general assembly took place on Monday, June 21st 2021 at 13:00 CEST. The meeting was held online as a password protected open source video conference [BigBlueButton](https://bigbluebutton.org/). The system possesses a chat feature. The agenda and presentations were managed and provided via the open source tool [OpenSlides](https://openslides.com/). OpenSlides was also used to register the presence of e.V. members, to manage voting proxies, and to execute the votes. This system ensures secure, secret, and verifiable voting. Both systems are operated on KDE servers. All e.V. members got access to both systems in advance. The list of participants was exported from OpenSlides for documentation purposes.

At the beginning, 90 members are present. Seven members who are not present have named a proxy. There are no non-voting members present. Petra Gillert, assistant to the board, participates in the event as guest.


## Agenda

1. Welcome
2. Election of the meeting chairperson
3. Reports from the board
   1. Report on activities
   2. Report from the treasurer
   3. Report from the auditors of accounting
   4. Relief of the board
4. Report from representatives and working groups
   1. Report from the representatives to the KDE Free Qt Foundation
   2. Report from the Advisory Board Working Group
   3. Report from the Community Working Group
   4. Report from the Financial Working Group
   5. Report from the KDE Free Qt Working Group
   6. Report from the Fundraising Working Group
   7. Report from the Onboarding Working Group
   8. Report from the System Administration Working Group
5. Election of auditors of accounting
6. Election of representatives to the KDE Free Qt Foundation
7. Votes proposed by members
   1. Appointing a director
   2. Fiduciary License Agreement 2.0
8. Miscellaneous discussion

## Minutes

All reports were sent to the members in advance per email, and were publicly presented on June 19th 2021 at the [Akademy](https://akademy.kde.org/2021) conference, organised by the e.V.

### Welcome and election of chairperson for the meeting

At 13:00 the meeting is opened by the president of the board, Aleix Pol. He thanks the organisers of the General Assembly.

Frederik Gladhorn is elected as chairperson for the meeting by general acclaim.

The chairperson notes that invitations to the General Assembly were sent out in the proper fashion and in a timely manner - by sending an email on May 9th 2021 containing the preliminary agenda and again on June 7th 2021 containing the final agenda. A short round of questions ensues because a few members presumably did not get the invitation. This was resolved. There were no objections.

The agenda for the meeting was acknowledged by general acclaim.

The chairperson appoints Thomas Baumgart as record keeper of the minutes and notes that the statutory quorum was attained.

In the beginning there are some technical problems with the tool Openslides, which led to various members not being able to log in to the voting system. This is fixed by the system administrators behind the scenes before the first vote.

### Report of the board

#### Report on activities

The current board consists of Aleix Pol i Gonzàlez (chairperson of the board), Eike Hein (treasurer and deputy chairperson of the board), Lydia Pintscher (deputy chairperson of the board), Adriaan de Groot and Neofytos Kolokotronis.

23 new active members have joined the e.V. since the last General Assembly.

The e.V. again lost some supporting members. Compared to 84 last year there are only 71 this year. The board has started quarterly informing its supporting members about its work by newsletter to prevent further fluctuation. The problems with CiviCRM and payments via PayPal persist. Two new patrons joined, causing the advisory board, which consists of KDE patrons and elected community partners, to be extended.

The e.V. has one direct employee and six freelance contractors. Petra Gillert is a direct employee in the capacity of assistant to the board, Aniqa Khokhar and Paul Brown work as marketing consultants, Adam Szopa as project coordinator, and Allyson Alexandrou as event coordinator. A short time ago Frederik Schwarzer and Carl Schwan joined to do documenation work. There are additional job offers for the projct 'Blauer Engel'. The areas of responsibility of the previously mentioned are presented.

As far as possible, Akademy 2022 will be planned as in-person conference.

Aleix Pol presents a detailed report regarding the status of individual activities the board worked on during the last year and presents an outlook of activities for the year to come. KDE celebrates its 25th birthday on October 14th 2021. The board asks for ideas from the members, how the anniversary can be used and celebrated.

#### Report from the Treasurer

Eike Hein presents the Report from the Treasurer, which was written with the help of the Financial Working Group and the members of the board.

##### Fiscal year 2020

Because of the uncertainty regarding the willingness to pay of supporting members and donors that was caused by the Corona pandemic, a conservative policy of spending was chosen to avoid financial risks. These fears, however, did not materialize. An additional large donation by the Handshake Foundation enabled us to maintain a stable financial situation, with revenues again surpassing expenses, in spite of reduced income from sponsorship programs like Google Summer of Code.

Eike presents some details regarding revenues and expenses with the aid of graphical evaluations.

##### Fiscal plan 2021

For 2021 Eike expects a stable revenue situation. There are no larger events planned for 2021. Therefore, travel expenditure is not expected to be high. He outlines planned expenses for freelancers to be additionally booked in multiple areas of expertise and projects. These pursuits should provide for more visibility of the e.V. in society. The board anticipates higher revenues through grants and donations as a result in the future.

The price for supporting memberships and sponsorships are a sensitive area that can only be adjusted very carefully. The topic of fundraising therefore prospectively gains higher importance in sustaining the activities of the e.V. in their usual manner.

Subsequently, questions from the membership about the reports are answered.

#### Report of the Auditors of Accounting

The auditors of accounting have met online this year with Lydia Pintscher and Petra Gellert and have examined the books. Proper accounting is attested. All question of the auditors of accounting were answered.

The auditors of accounting recommend to the membership that the board be relieved for the fiscal year 2020.

#### Relief of the Board

The chairperson explains the meaning of "relief of the board" and asks to report objections agains the relief. There were no objections.

The vote is done using the now fully functional voting feature of the online tool Openslides. The members of the board do not participate in this vote. The results of the vote are:

* In favor of the relief:     **83**
* Against the relief:         **1**
* Abstentions:                **3**
* Total Participants:         **87**

The board is declared relieved by the result of this vote.

### Report of the representatives and working groups of the e.V.

During this part the respective groups only give additions that are not meant for the general public and therefore have not been included to the previously sent reports and public presentations.

#### Report of the Representatives to the KDE Free Qt Foundation

Olaf Schmidt-Wischhöfer reports as representative for KDE e.V. from the KDE Free Qt Foundation. The formal registration of the new presidency of the foundation has not yet occured. This is due to the slowed processing by the Norwegian authorities caused by the Corona pandemic.

Both former representatives Olaf Schmidt-Wischhöfer and Martin Konold are very busy professionally. They would therefore like for someone new to stand for election. Frederik Gladhorn thanks both of them for their longstanding and excellent work in the interest of the e.V.

#### Other groups

The other groups had no information to add beyond the publicly available information.

### Election of the Auditors of Accounting

Both former auditors of accounting Andreas Cord-Landwehr and Ingo Klöcker are available for reelection as auditors of accounting. There are no additional suggestions. The cast votes yield the following results:

* Andreas Cord-Landwehr:   **91 yes**
* Ingo Klöcker:            **91 yes**
* Abstentions:             **6**
* Participants:            **97**

Both candidates accept the election when asked by the chairperson.

### Election of the Representatives to the KDE Free Qt Foundation

Olaf Schmidt-Wischhöfer is available for reelection. Albert Astals Cid comes forward as additional candidate. Luis Falcon retracts his candidacy before the ballot. There are no additional nominations. The members have the choice of voting for one or both candidates, or to abstain.

Thus, the candidates for the election of the representatives of the KDE e.V. to the KDE Free Qt Foundation are:

* Olaf Schmidt-Wischhöfer:    **90 yes**
* Albert Astals Cid:          **85 yes**
* Abstentions:                **4**
* Participants:               **96**

Both candidates accept the election when asked by the chairperson.

### Motions requested by Members

The two motions for votes submitted by the members were received by the Board in due time.

#### Hiring a Managing Director

The motion by Cornelius Schumacher on the "recommendation of the membership to the board to hire a managing director" was made available to the members in advance of the assembly by email, and also discussed by the members on the mailing list prior to the assembly. Cornelius states the reasons for his motion at the assembly. Aleix Pol states the current opinion of the board regarding the topic by the means of a presentation. He encourages the members to themselves get active to relieve the board, in order to avoid hiring a managing director for now. After a short discussion, the motion is voted on as follows:

* In favor of the motion:      **27**
* Against the motion:          **35**
* Abstentions:                 **30**
* Participants:                **92**

Thus, the motion is rejected by the membership.

#### Fiduciary License Agreement 2.0

The motion was retracted by its mover Adriaan de Groot because the new version of the agreement is not yet available.

### Miscellaneous

No points were discussed

### Closing of the Assembly

Frederik Gladhorn closes the assembly at 15:46 and asks forgiveness for the technical problems at the beginning of the assembly. Aleix Pol closes the general assembly at 15:49 and thanks Frederik for his role as chairperson.


The assembly was held free from sound or video interruptions.


Thomas Baumgart            Frederik Gladhorn
(Keeper of the minutes)    (Chairperson)
23.06.2021                 23.06.2021
