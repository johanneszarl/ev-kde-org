---
title: "KDE e.V. Members"
menu:
  main:
    parent: organization
    weight: 3
    name: Members
scssFiles:
- /css/main.scss
---

If you are an active member of the KDE community and would like to join the KDE
e.V. please have a look at the <a href="/getinvolved/members/">information
how to become a member of the KDE e.V.</a>

These are the current active members (alphabetical by first name):

<noscript><p>This list only works if JavaScript is enabled.</p></noscript>

{{< members >}}


<br style="clear:left;" />
